import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from '../../chat.service';
import { startWith } from 'rxjs/operators';
import { interval } from 'rxjs';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit {

  username: string;
  message: string;
  feeds: any;
  intervalObj: any;

  constructor(public route: Router, public chatsvc: ChatService) {
    if (localStorage.getItem('username') === null) {
      this.route.navigateByUrl('/login');
    }
  }

  ngOnInit() {
    this.intervalObj = interval(3000).pipe(startWith(0))
    .subscribe(() => {
    this.getFeed();
    this.getFeed();
    });
  }

  postfeed() {
    this.chatsvc.postfeed(this.username, this.message)
    .subscribe(resp => {
      console.log(resp);
    });
    }
    getFeed() {
      this.chatsvc.getfeed().subscribe(resp => {
        this.feeds = resp;
        this.feeds.reverse();
      });
    }
  }
