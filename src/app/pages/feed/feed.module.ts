import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeedRoutingModule } from './feed-routing.module';
import { FeedComponent } from './feed.component';
import { CardComponent } from './components/card/card.component';


@NgModule({
  declarations: [FeedComponent, CardComponent],
  imports: [
    CommonModule,
    FeedRoutingModule
  ]
})
export class FeedModule { }
